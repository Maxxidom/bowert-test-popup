/**
 *  maxxidom Popup v1.5.3
 *  Autor: Alexander Bechthold
 *  Date: 02.10.2017
 *  Licensed under the MIT license
 */


function MaxxidomPopup(options) {

    this.modal = document.querySelector(options.modal);
    this.overlay = document.querySelector(options.overlay);

    var popup = this;

    this.open = function(content) {
        popup.modal.innerHTML = content;
        popup.modal.classList.add('open');
        popup.overlay.classList.add('open');
        document.body.style.overflowY = 'hidden';
        document.body.style.marginLeft = '-5px';
    };

    this.close = function() {
      popup.modal.classList.remove('open');
      popup.overlay.classList.remove('open');
      document.body.style.overflowY = 'auto';
      document.body.style.marginLeft = '0';
    };

    this.overlay.onclick = popup.close;

}